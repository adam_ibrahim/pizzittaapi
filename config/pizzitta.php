<?php



return [

    /*
    |--------------------------------------------------------------------------
    | Shipping cost
    |--------------------------------------------------------------------------
    |
    | Cost of shipping can be inserted to ENV file default is 1 Euro
    |
    */

    'shipping' => env('shipping', 1),

];
