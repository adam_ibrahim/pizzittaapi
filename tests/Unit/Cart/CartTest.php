<?php

namespace Tests\Unit\Item;

use App\Cart;
use App\Item;
use App\User;
use Tests\TestCase;

class CartTest extends TestCase
{

    public function test_it_can_add_items_to_cart()
    {
        $cart = new Cart(
            $user = factory(User::class)->create()
        );

        $item = factory(Item::class)->create();

        $cart->add([
           ['id' => $item->id, 'quantity' => 1]
        ]);

        $this->assertEquals(1, $user->fresh()->cart->count());
    }


    public function test_it_increment_quantity_when_adding_new_item()
    {
        $item = factory(Item::class)->create();

        $cart = new Cart(
            $user = factory(User::class)->create()
        );

        $cart->add([
            ['id' => $item->id, 'quantity' => 1]
        ]);

        $cart = new Cart($user->fresh());

        $cart->add([
            ['id' => $item->id, 'quantity' => 1]
        ]);

        $this->assertEquals(2, $user->fresh()->cart->first()->pivot->quantity);
    }

    public function test_it_can_update_quantities_in_cart()
    {

        $cart = new Cart(
            $user = factory(User::class)->create()
        );

        $user->cart()->attach(
            $item = factory(Item::class)->create(), [
            'quantity' => 1
        ]);

        $cart->update($item->id, 2);

        $this->assertEquals(2, $user->fresh()->cart->first()->pivot->quantity);
    }

    public function test_it_can_delete_an_item_from_cart()
    {

        $cart = new Cart(
            $user = factory(User::class)->create()
        );

        $user->cart()->attach(
            $item = factory(Item::class)->create());

        $cart->delete($item->id);

        $this->assertCount(0, $user->fresh()->cart);
    }


    public function test_it_can_empty_the_cart()
    {

        $cart = new Cart(
            $user = factory(User::class)->create()
        );

        $user->cart()->attach(
            $item = factory(Item::class)->create());

        $cart->empty();

        $this->assertCount(0, $user->fresh()->cart);
    }
}
