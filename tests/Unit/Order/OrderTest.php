<?php

namespace Tests\Unit\Order;

use App\Item;
use App\Order;
use Tests\TestCase;

class OrderTest extends TestCase
{

    public function test_it_belongs_to_many_items()
    {
        $order = factory(Order::class)->create();

        $order->items()->attach(factory(Item::class)->create());

        $this->assertInstanceOf(Item::class, $order->items()->first());
    }

    public function test_it_has_item_quantity_pivot()
    {
        $order = factory(Order::class)->create();

        $order->items()->attach(factory(Item::class)->create(), [
            'quantity' => $quantity = 5
        ]);

        $this->assertEquals($quantity, $order->items()->first()->pivot->quantity);
    }
}
