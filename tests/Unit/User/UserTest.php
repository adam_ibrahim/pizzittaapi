<?php

namespace Tests\Unit\User;

use App\Item;
use App\Order;
use App\User;
use App\UserContact;
use Tests\TestCase;

class UserTest extends TestCase
{

    public function test_it_has_many_contacts()
    {
        $user = factory(User::class)->create();

        $user->contacts()->save(
            factory(UserContact::class)->create()
        );

        $this->assertInstanceOf(UserContact::class , $user->contacts->first());
    }


    public function test_it_has_many_cart_items()
    {
        $user = factory(User::class)->create();

        $user->cart()->attach(
            factory(Item::class)->create()
        );

        $this->assertInstanceOf(Item::class , $user->cart->first());
    }


    public function test_it_has_a_quantity_for_each_cart_item()
    {
        $user = factory(User::class)->create();

        $user->cart()->attach(
            factory(Item::class)->create(),[
                'quantity' => $quantity = 5
            ]
        );

        $this->assertEquals($user->cart->first()->pivot->quantity, 5);
    }


    public function test_it_has_orders()
    {
        $user = factory(User::class)->create();

        $user->orders()->save(factory(Order::class)->create());

        $this->assertInstanceOf(Order::class, $user->orders->first());
    }
}
