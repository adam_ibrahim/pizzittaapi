<?php

namespace Tests\Feature\Order;

use App\Cart;
use App\Currency;
use App\Item;
use App\Order;
use App\User;
use App\UserContact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderCreateTest extends TestCase
{


    public function test_it_fails_if_unauthenticated()
    {
        $this->json('POST', 'api/orders')
            ->assertStatus(401);
    }

    public function test_it_fails_if_cart_is_empty()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/orders')
            ->assertStatus(422);
    }

    public function test_it_requires_contact_id()
    {
        $cart = new Cart(
            $user = factory(User::class)->create()
        );

        $item = factory(Item::class)->create();

        $cart->add([
            ['id' => $item->id, 'quantity' => 1]
        ]);

        $this->actingAs($user)->json('POST', 'api/orders')
            ->assertJsonValidationErrors(['contact_id']);
    }

    public function test_it_requires_contact_id_to_be_numeric()
    {
        $cart = new Cart(
            $user = factory(User::class)->create()
        );

        $item = factory(Item::class)->create();

        $cart->add([
            ['id' => $item->id, 'quantity' => 1]
        ]);

        $this->actingAs($user)->json('POST', 'api/orders', [
            'contact_id' => 'invalid'])

        ->assertJsonValidationErrors(['contact_id']);
    }

    public function test_it_requires_contact_id_to_exist()
    {
        $cart = new Cart(
            $user = factory(User::class)->create()
        );

        $item = factory(Item::class)->create();

        $cart->add([
            ['id' => $item->id, 'quantity' => 1]
        ]);

        $this->actingAs($user)->json('POST', 'api/orders', [
            'contact_id' => 1])

            ->assertJsonValidationErrors(['contact_id']);
    }

    public function test_it_requires_contact_id_to_belong_to_auth()
    {
        $cart = new Cart(
            $user = factory(User::class)->create()
        );

        $item = factory(Item::class)->create();

        $cart->add([
            ['id' => $item->id, 'quantity' => 1]
        ]);

        $user_2 = factory(User::class)->create();

        $contact = $user_2->contacts()->save(
            factory(UserContact::class)->create()
        );

        $this->actingAs($user)->json('POST', 'api/orders', [
            'contact_id' => $contact->id])

            ->assertJsonValidationErrors(['contact_id']);
    }

    public function test_it_creates_the_order()
    {
        $cart = new Cart(
            $user = factory(User::class)->create()
        );

        $item = factory(Item::class)->create();

        $cart->add([
            ['id' => $item->id, 'quantity' => 1]
        ]);

        $contact = $user->contacts()->save(
            factory(UserContact::class)->create()
        );

        factory(Currency::class)->create();

        $this->actingAs($user)->json('POST', 'api/orders', [
            'contact_id' => $contact->id ]);

            $this->assertDatabaseHas('orders', [
                'user_id' => $user->id,
                'contact_id' => $contact->id
            ]);

    }

    public function test_order_completed_event_updates_the_status()
    {
        $cart = new Cart(
            $user = factory(User::class)->create()
        );

        $item = factory(Item::class)->create();

        $cart->add([
            ['id' => $item->id, 'quantity' => 1]
        ]);

        $contact = $user->contacts()->save(
            factory(UserContact::class)->create()
        );

        factory(Currency::class)->create();

        $this->actingAs($user)->json('POST', 'api/orders', [
            'contact_id' => $contact->id ]);

        $this->assertDatabaseHas('orders', [
            'status' => Order::ON_ITS_WAY,
        ]);
    }

    public function test_order_completed_event_empty_cart()
    {
        $cart = new Cart(
            $user = factory(User::class)->create()
        );

        $item = factory(Item::class)->create();

        $cart->add([
            ['id' => $item->id, 'quantity' => 1]
        ]);

        $contact = $user->contacts()->save(
            factory(UserContact::class)->create()
        );

        factory(Currency::class)->create();

        $this->actingAs($user)->json('POST', 'api/orders', [
            'contact_id' => $contact->id ]);

        $this->assertEquals(0, $user->cart()->count());
    }

}
