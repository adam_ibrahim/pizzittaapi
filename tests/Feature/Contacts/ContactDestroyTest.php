<?php

namespace Tests\Feature\Cart;

use App\User;
use App\UserContact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactDestroyTest extends TestCase
{

    public function test_it_fails_if_unauthenticated()
    {
        $user = factory(User::class)->create();

        $contact = $user->contacts()->save(factory(UserContact::class)->create());

        $this->json('DELETE', 'api/contact/' . $contact->id)
            ->assertStatus(401);
    }


    public function test_it_fails_if_contact_not_found()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('DELETE', 'api/contact/invalid_id')
            ->assertStatus(404);
    }


    public function test_it_deletes_contact()
    {
        $user = factory(User::class)->create();

        $contact = $user->contacts()->save(
            factory(UserContact::class)->create()
        );

        $this->actingAs($user)->json('DELETE', 'api/contact/' . $contact->id);

        $this->assertDatabaseMissing('user_contacts', [
            'id' => $contact->id,
        ]);
    }

}
