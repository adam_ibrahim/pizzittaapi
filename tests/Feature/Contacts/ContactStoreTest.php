<?php

namespace Tests\Feature\Cart;

use App\User;
use App\UserContact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactStoreTest extends TestCase
{

    public function test_it_fails_if_unauthenticated()
    {
        $this->json('POST', 'api/contact')
            ->assertStatus(401);
    }

    public function test_it_requires_phone()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/contact')
            ->assertJsonValidationErrors(['phone']);
    }

    public function test_it_requires_address()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/contact')
            ->assertJsonValidationErrors(['address_1']);
    }

    public function test_it_requires_city()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/contact')
            ->assertJsonValidationErrors(['city']);
    }

    public function test_it_requires_state()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/contact')
            ->assertJsonValidationErrors(['state']);
    }

    public function test_it_requires_postal_code()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/contact')
            ->assertJsonValidationErrors(['postal_code']);
    }

    public function test_it_requires_country()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/contact')
            ->assertJsonValidationErrors(['country']);
    }

    public function test_it_set_all_contacts_default_false_if_new_default()
    {
        $user = factory(User::class)->create();

        $contact = $user->contacts()->save(
            factory(UserContact::class)->create([
                'default' => true
            ])
        );

        $this->actingAs($user)->json('POST', 'api/contact', $this->contactData() );

        $this->assertEquals(0, $contact->fresh()->default);
    }


    public function test_it_inserts_new_user_contact()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/contact', $this->contactData() );

        $this->assertDatabaseHas('user_contacts', [
            'phone' => $this->contactData()['phone'],
            'user_id' => $user->id,
        ]);
    }


    /**
     * @return array
     */
    protected function contactData()
    {
        return [
            'phone'=>'79126916242',
            'title'=>'new contact',
            'address_1'=>'12 new address',
            'city'=>'Yekaterinburg',
            'state'=>'Sverdlovisky Oblast',
            'postal_code'=>'620075',
            'country'=>'Russian',
            'default'=>true,
        ];
    }


}
