<?php

namespace Tests\Feature\Cart;

use App\User;
use App\UserContact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactUpdateTest extends TestCase
{

    public function test_it_fails_if_unauthenticated()
    {
        $user = factory(User::class)->create();

        $contact = $user->contacts()->save(factory(UserContact::class)->create());

        $this->json('PATCH', 'api/contact/' . $contact->id)
            ->assertStatus(401);
    }

    public function test_it_fails_if_contact_not_found()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('PATCH', 'api/contact/invalid_id')
            ->assertStatus(404);
    }


    public function test_it_requires_phone()
    {
        $user = factory(User::class)->create();

        $contact = $user->contacts()->save(factory(UserContact::class)->create());

        $this->actingAs($user)->json('PATCH', 'api/contact/' . $contact->id)
            ->assertJsonValidationErrors(['phone']);
    }

    public function test_it_requires_address()
    {
        $user = factory(User::class)->create();

        $contact = $user->contacts()->save(factory(UserContact::class)->create());

        $this->actingAs($user)->json('PATCH', 'api/contact/' . $contact->id)
            ->assertJsonValidationErrors(['address_1']);
    }

    public function test_it_requires_city()
    {
        $user = factory(User::class)->create();

        $contact = $user->contacts()->save(factory(UserContact::class)->create());

        $this->actingAs($user)->json('PATCH', 'api/contact/' . $contact->id)
            ->assertJsonValidationErrors(['city']);
    }

    public function test_it_requires_state()
    {
        $user = factory(User::class)->create();

        $contact = $user->contacts()->save(factory(UserContact::class)->create());

        $this->actingAs($user)->json('PATCH', 'api/contact/' . $contact->id)
            ->assertJsonValidationErrors(['state']);
    }

    public function test_it_requires_postal_code()
    {
        $user = factory(User::class)->create();

        $contact = $user->contacts()->save(factory(UserContact::class)->create());

        $this->actingAs($user)->json('PATCH', 'api/contact/' . $contact->id)
            ->assertJsonValidationErrors(['postal_code']);
    }

    public function test_it_requires_country()
    {
        $user = factory(User::class)->create();

        $contact = $user->contacts()->save(factory(UserContact::class)->create());

        $this->actingAs($user)->json('PATCH', 'api/contact/' . $contact->id)
            ->assertJsonValidationErrors(['country']);
    }

    public function test_it_set_all_contacts_default_false_if_new_default()
    {
        $user = factory(User::class)->create();

        $contact_1 = $user->contacts()->save(
            factory(UserContact::class)->create([
                'default' => true
            ])
        );

        $contact_2 = $user->contacts()->save(
            factory(UserContact::class)->create()
        );

        $this->actingAs($user)->json('PATCH', 'api/contact/' . $contact_2->id, $this->contactData() );

        $this->assertEquals(0, $contact_1->fresh()->default);
    }


    public function test_it_updates_contact()
    {
        $user = factory(User::class)->create();

        $contact = $user->contacts()->save(
            factory(UserContact::class)->create()
        );

        $this->actingAs($user)->json('PATCH', 'api/contact/' . $contact->id, $this->contactData() );

        $this->assertDatabaseHas('user_contacts', [
            'phone' => $this->contactData()['phone'],
            'user_id' => $user->id,
        ]);
    }


    /**
     * @return array
     */
    protected function contactData()
    {
        return [
            'phone'=>'79126916242',
            'title'=>'new contact',
            'address_1'=>'12 new address',
            'city'=>'Yekaterinburg',
            'state'=>'Sverdlovisky Oblast',
            'postal_code'=>'620075',
            'country'=>'Russian',
            'default'=>true,
        ];
    }


}
