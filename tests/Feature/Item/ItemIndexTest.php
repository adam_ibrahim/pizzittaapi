<?php

namespace Tests\Feature\Item;

use App\Currency;
use App\Item;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ItemIndexTest extends TestCase
{

    public function test_it_shows_a_collection_of_items()
    {
        factory(Currency::class)->create();

        $item = factory(Item::class)->create();

        $this->json('GET', 'api/items')->assertJsonFragment([
            'id' => $item->id
        ]);
    }

    public function test_it_has_paginated_data()
    {
        factory(Item::class, 20)->create();

        factory(Currency::class)->create();

       $this->json('GET', 'api/items')->assertJsonStructure([
            'meta', 'links'
        ]);
    }
}
