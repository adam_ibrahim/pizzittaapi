<?php

namespace Tests\Feature\Auth;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MeTest extends TestCase
{


    public function test_it_fails_if_unauthenticated()
    {
        $this->json('GET', 'api/auth/me')
            ->assertStatus(401);
    }

    public function test_it_returns_user_resource_if_authenticated()
    {
        $user = factory(User::class)->create();

        $token = auth()->tokenById($user->id);

        $this->json('GET', 'api/auth/me', [], [
             'Authorization' => 'Bearer ' . $token
            ])->assertJsonFragment([
                'email' => $user->email
        ]);

    }

}
