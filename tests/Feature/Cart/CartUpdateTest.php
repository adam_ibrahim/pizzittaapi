<?php

namespace Tests\Feature\Cart;

use App\Item;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CartUpdateTest extends TestCase
{


    public function test_it_fails_if_unauthenticated()
    {
        $this->json('PATCH', 'api/cart/1')
            ->assertStatus(401);
    }

    public function test_it_fails_if_item_not_found()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('PATCH', 'api/cart/1')
            ->assertStatus(404);
    }

    public function test_it_requires_quantity()
    {
        $user = factory(User::class)->create();

        $item = factory(Item::class)->create();

        $this->actingAs($user)->json('PATCH', 'api/cart/' . $item->id)
            ->assertJsonValidationErrors(['quantity']);
    }

    public function test_it_requires_numeric_quantity()
    {
        $user = factory(User::class)->create();

        $item = factory(Item::class)->create();

        $this->actingAs($user)->json('PATCH', 'api/cart/' . $item->id, [
            'quantity' => 'One'
        ])
            ->assertJsonValidationErrors(['quantity']);
    }

    public function test_it_requires_quantity_min_1()
    {
        $user = factory(User::class)->create();

        $item = factory(Item::class)->create();

        $this->actingAs($user)->json('PATCH', 'api/cart/' . $item->id, [
            'quantity' => 0
        ])
            ->assertJsonValidationErrors(['quantity']);
    }



    public function test_it_updates_cart()
    {
        $user = factory(User::class)->create();

        $user->cart()->attach(
            $item = factory(Item::class)->create(), [
                'quantity' => 1
            ]
        );

        $this->actingAs($user)->json('PATCH', 'api/cart/' . $item->id, [
            'quantity' => $quantity = 5
        ]);

        $this->assertDatabaseHas('item_user', [
            'user_id' => $user->id,
            'item_id' => $item->id,
            'quantity' => $quantity
        ]);
    }
}
