<?php

namespace Tests\Feature\Cart;

use App\Item;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CartDestroyTest extends TestCase
{


    public function test_it_fails_if_unauthenticated()
    {
        $this->json('DELETE', 'api/cart/1')
            ->assertStatus(401);
    }


    public function test_it_fails_if_item_not_found()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('DELETE', 'api/cart/1')
            ->assertStatus(404);
    }


    public function test_it_deletes_an_item_from_the_cart()
    {
        $user = factory(User::class)->create();

        $user->cart()->attach(
            $item = factory(Item::class)->create(), [
                'quantity' => 1
            ]
        );

        $this->actingAs($user)->json('DELETE', 'api/cart/' . $item->id);

        $this->assertDatabaseMissing('item_user', [
            'item_id' => $item->id
        ]);
    }
}
