<?php

namespace Tests\Feature\Cart;

use App\Currency;
use App\Item;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CartIndexTest extends TestCase
{


    public function test_it_fails_if_unauthenticated()
    {
        $this->json('GET', 'api/cart')
            ->assertStatus(401);
    }


    public function test_it_return_the_cart_item()
    {
        $user = factory(User::class)->create();

        $user->cart()->sync(
           $item = factory(Item::class)->create()
        );

        factory(Currency::class)->create();

        $this->actingAs($user)->json('GET', 'api/cart')
            ->assertJsonFragment([
                'id' => $item->id
            ]);
    }

    public function test_it_shows_correct_total_cost()
    {
        $user = factory(User::class)->create();

        $item = factory(Item::class)->create([
            'price' => $price = 3
        ]);

        $user->cart()->sync([
            $item->id => [
                'quantity' => $quantity = 5
            ]
        ]);


        $currency = factory(Currency::class)->create();

        $this->actingAs($user)->json('GET', 'api/cart')
            ->assertJsonFragment([
                'total_cost' => $currency->sign.''.number_format($price * $quantity, 2)
            ]);
    }
}
