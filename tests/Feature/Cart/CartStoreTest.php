<?php

namespace Tests\Feature\Cart;

use App\Item;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CartStoreTest extends TestCase
{


    public function test_it_fails_if_unauthenticated()
    {
        $this->json('POST', 'api/cart')
            ->assertStatus(401);
    }

    public function test_it_requires_items()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/cart')
            ->assertJsonValidationErrors(['items']);
    }

    public function test_it_requires_items_to_be_an_array()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/cart', [
            'items' => 1
            ])
            ->assertJsonValidationErrors(['items']);
    }

    public function test_it_requires_items_exists()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/cart', [
            'items' => [
                ['id'=>1, 'quantity' => 1]
            ]
        ])
            ->assertJsonValidationErrors(['items.0.id']);
    }

    public function test_it_requires_items_quantity_to_be_min_1()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/cart', [
            'items' => [
                ['id'=>1, 'quantity' => 0 ]
            ]
        ])
            ->assertJsonValidationErrors(['items.0.quantity']);
    }

    public function test_it_requires_items_quantity_to_be_numeric()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->json('POST', 'api/cart', [
            'items' => [
                ['id'=>1, 'quantity' => 'one']
            ]
        ])
            ->assertJsonValidationErrors(['items.0.quantity']);
    }

    public function test_it_can_add_items_to_users_cart()
    {
        $user = factory(User::class)->create();

        $item = factory(Item::class)->create();

        $this->actingAs($user)->json('POST', 'api/cart', [
            'items' => [
                ['id'=> $item->id, 'quantity' => $quantity = 5]
            ]
        ]);

        $this->assertDatabaseHas('item_user', [
           'item_id' => $item->id,
           'quantity' => $quantity,
           'user_id' => $user->id,
        ]);
    }
}
