<?php

use Illuminate\Database\Seeder;
use App\Currency;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Currency::truncate();

        DB::table('currencies')->insert([
            'title' => 'Euro',
            'code' => 'EUR',
            'sign' => '€',
            'rate' => 1,
            'default' => true
        ]);

        DB::table('currencies')->insert([
            'title' => 'USD',
            'code' => 'USD',
            'sign' => '$',
            'rate' => 1.12,
            'default' => false
        ]);
    }
}
