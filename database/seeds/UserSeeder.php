<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::truncate();

        DB::table('users')->insert([
            'name' => 'Adam Ibrahim',
            'email' => 'info@pizzitta.test',
            'password' => bcrypt('password')
        ]);

    }
}
