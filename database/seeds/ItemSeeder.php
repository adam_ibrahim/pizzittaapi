<?php

use App\Item;
use Illuminate\Database\Seeder;


class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Item::truncate();
        factory(Item::class, 10)->create();
    }
}
