<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UserContact;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(UserContact::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'phone' => $faker->phoneNumber,
        'address_1' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->state,
        'postal_code' => $faker->postcode,
        'country' => $faker->country,
    ];
});
