<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Currency;
use Faker\Generator as Faker;

$factory->define(Currency::class, function (Faker $faker) {
    return [
        'title' => $faker->currencyCode,
        'code' => $faker->currencyCode,
        'sign' => $faker->currencyCode,
        'default' => true,
    ];
});
