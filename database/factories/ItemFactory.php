<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Item;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {

    return [
        'title' => $title = $faker->unique()->name,
        'slug' => Str::slug($title),
        'price' => rand(3, 5),
        'description' => $faker->sentence(5),
        'image' => rand(1, 10).'.jpeg',
    ];

});
