<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{

    /**
     * @param $amount
     * @return float|int
     */
    public static function convertAmount($amount)
    {

        return implode('', [
            self::currentCurrency()->sign,
            number_format(($amount * self::currentCurrency()->rate), 2),

        ]);
    }

    /**
     * @return mixed
     */
    public static function currentCurrency()
    {
        if(!request()->header('currency')) {
            return Currency::where('default', 1)->first();
        }

        if (!$session_currency = Currency::find(request()->header('currency')))
        {
            return Currency::where('default', 1)->first();
        }

        return $session_currency;
    }
}
