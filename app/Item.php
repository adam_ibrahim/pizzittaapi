<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * @var array
     */
    protected $guarded = ['id'];

}
