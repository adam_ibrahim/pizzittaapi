<?php

namespace App\Listeners;

use App\Cart;
use App\Events\OrderCompleted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class EmptyCart
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderCompleted  $event
     * @return void
     */
    public function handle(OrderCompleted $event)
    {

        $cart = new Cart(auth()->user());

        $orders = $cart->items()->keyBy('id')->map(function($item){
            return [
                'quantity' => $item->pivot->quantity
            ];
        })->toArray();

       $event->order->items()->sync($orders);

        $cart->empty();
    }
}
