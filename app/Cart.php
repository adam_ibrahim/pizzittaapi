<?php

namespace App;

class Cart {

    /**
     * @var User
     */
    protected $user;

    /***
     * Cart constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /***
     * @return mixed
     */
    public function items(){
         return $this->user->cart;
    }

    /**
     * @param $items
     */
    public function add($items)
    {
        $this->user->cart()->syncWithoutDetaching(
            $this->getStorePayload($items)
        );
    }

    /**
     * @param $itemId
     * @param $quantity
     */
    public function update($itemId, $quantity)
    {
        $this->user->cart()->updateExistingPivot($itemId, [
            'quantity' => $quantity
        ]);
    }

    /**
     * @param $itemId
     */
    public function delete($itemId)
    {
        $this->user->cart()->detach($itemId);
    }

    /**
     *
     */
    public function empty()
    {
        $this->user->cart()->detach();
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return $this->user->cart->sum('pivot.quantity') <= 0;
    }

    /**
     * @return mixed
     */
    public function subtotal()
    {
        return $this->user->cart->sum( function($query) {
            return $query->price * $query->pivot->quantity;
        });
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    public function shipping()
    {
        return config('pizzitta.shipping');
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    public function total()
    {
        return ($this->subtotal() + $this->shipping());
    }

    /**
     * @param $items
     * @return array
     */
    protected function getStorePayload($items)
    {
        return collect($items)->keyBy('id')->map(function ($item){
            return [
                'quantity' => $item['quantity'] + $this->getCurrentQuantity($item['id'])
            ];

        })
            ->toArray();
    }

    protected function getCurrentQuantity($itemId)
    {
        if($item = $this->user->cart->where('id', $itemId)->first()) {
            return $item->pivot->quantity;
        }
    }

}
