<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{
    /**
     * Unfillable items
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
