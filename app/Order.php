<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const ON_ITS_WAY= 'on_its_way';
    const DELIVERED = 'delivered';
    const CANCELED = 'canceled';
    const UNPAID = 'unpaid';


    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function items()
    {
        return $this->belongsToMany('App\Item')->withPivot('quantity');
    }
}
