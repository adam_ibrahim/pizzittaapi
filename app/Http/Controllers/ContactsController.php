<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserContactFormRequest;
use App\Http\Resources\UserContactsResource;
use App\UserContact;
use Illuminate\Http\Request;

class ContactsController extends Controller
{

    /**
     * ContactsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Insert new Contact details
     * @param UserContactFormRequest $request
     * @return UserContactsResource
     */
    public function store(UserContactFormRequest $request)
    {
        $this->userContactDefault($request);

        $contact = auth()->user()->contacts()->create($request->all());

        return new UserContactsResource($contact);
    }

    /**
     * @param UserContact $contact
     * @param UserContactFormRequest $request
     * @return UserContactsResource
     */
    public function update(UserContact $contact, UserContactFormRequest $request)
    {
        $this->userContactDefault($request);

        $contact->update($request->all());

        return new UserContactsResource($contact);
    }


    /**
     * @param UserContact $contact
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(UserContact $contact)
    {
        $contact->delete();

        return response()->json([
            'message' => 'Deleted!'
        ], 200);
    }


    /**
     * @param Request $request
     */
    protected function userContactDefault(Request $request)
    {
        if($request->default) {
            auth()->user()->contacts()->update(['default'=>false]);
        }
    }
}
