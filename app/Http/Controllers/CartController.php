<?php

namespace App\Http\Controllers;
use App\Cart;
use App\Http\Requests\CartStoreRequest;
use App\Http\Requests\CartUpdateRequest;
use App\Http\Resources\CartResource;
use App\Item;
use Illuminate\Http\Request;

class CartController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:api');

    }

    /**
     * @param Request $request
     * @return CartResource
     */
    public function index(Request $request)
    {
        return new CartResource($request->user());
    }


    /**
     * @param CartStoreRequest $request
     * @param Cart $cart
     * @return CartResource
     */
    public function store(CartStoreRequest $request, Cart $cart)
    {
        $cart->add($request->items);
        return new CartResource($request->user());

    }


    /**
     * @param Item $item
     * @param CartUpdateRequest $request
     * @param Cart $cart
     * @return CartResource
     */
    public function update(Item $item, CartUpdateRequest $request, Cart $cart)
    {
        $cart->update($item->id, $request->quantity);
        return new CartResource($request->user());
    }

    /**
     * @param Item $item
     * @param Cart $cart
     * @return CartResource
     */
    public function destroy(Item $item,  Cart $cart)
    {
        $cart->delete($item->id);
        return new CartResource(auth()->user());
    }
}
