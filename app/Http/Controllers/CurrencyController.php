<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Http\Resources\CurrencyResource;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CurrencyResource::collection(Currency::get());
    }

    /**
     * @param Currency $currency
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function change(Currency $currency)
    {
        session()->put('currency', $currency->id);

        return CurrencyResource::collection(Currency::get());
    }
}
