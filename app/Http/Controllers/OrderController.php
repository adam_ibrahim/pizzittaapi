<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Events\OrderCompleted;
use App\Http\Requests\OrderCreateRequest;
use App\Http\Resources\OrderResource;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{


    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('cart')->only('store');

    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return OrderResource::collection(auth()->user()->orders);
    }

    /**
     * @param Order $order
     * @return OrderResource
     */
    public function show(Order $order)
    {
        return new OrderResource($order);
    }


    /**
     * @param Request $request
     * @param Cart $cart
     * @return OrderResource|\Illuminate\Http\JsonResponse
     */
    public function store(OrderCreateRequest $request, Cart $cart)
    {



        $order = $request->user()->orders()->create([
            'contact_id' => $request->contact_id,
            'subtotal' => $cart->subtotal(),
            'shipping_cost' => $cart->shipping(),
            'total' => $cart->total(),
        ]);

        event(new OrderCompleted($order));

        return new OrderResource($order);
    }
}
