<?php

namespace App\Http\Resources;

use App\Cart;
use App\Currency;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->created_at->format('d-m-Y H:i'),
            'items' => CartItemResource::collection($this->items),
            'subtotal' => Currency::convertAmount($this->subtotal),
            'shipping_cost' => Currency::convertAmount($this->shipping_cost),
            'total' => Currency::convertAmount($this->total),
            'status' => ucfirst(str_replace('_', ' ', $this->status))
        ];
    }

}
