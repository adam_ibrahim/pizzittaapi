<?php

namespace App\Http\Resources;

use App\Cart;
use App\Currency;
use Illuminate\Http\Resources\Json\JsonResource;
use test\Mockery\MockingVariadicArgumentsTest;

class CartResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'items' => CartItemResource::collection($this->cart),
            'summary' => $this->summary(new cart($request->user()))
        ];
    }


    protected function summary(Cart $cart)
    {
        return [
            'is_empty'  => $cart->isEmpty(),
            'subtotal'  => $cart->isEmpty() ? 0 : Currency::convertAmount($cart->subtotal()),
            'shipping'  => $cart->isEmpty() ? 0 : Currency::convertAmount($cart->shipping()),
            'total'     => $cart->isEmpty() ? 0 : Currency::convertAmount($cart->total()),
        ];
    }
}
