<?php

namespace App\Http\Resources;

use App\Currency;
use Illuminate\Http\Resources\Json\JsonResource;

class CartItemResource extends ItemResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
                'quantity' => $this->pivot->quantity,
                'total_cost' => $this->totalCost()
        ]);
    }

    /**
     * @return float|int
     */
    protected function totalCost()
    {
        return Currency::convertAmount(
            $this->pivot->quantity * $this->price
        );
    }
}
