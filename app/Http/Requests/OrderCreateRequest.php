<?php

namespace App\Http\Requests;

use App\Rules\AuthContactId;
use Illuminate\Foundation\Http\FormRequest;

class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact_id' => [
                'required',
                'numeric',
                'exists:user_contacts,id',
                new AuthContactId(),
            ],
        ];
    }
}
