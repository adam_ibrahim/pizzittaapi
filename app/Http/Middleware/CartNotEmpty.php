<?php

namespace App\Http\Middleware;

use App\Cart;
use Closure;

class CartNotEmpty
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->user()->cart()->count()) {
            return response()->json([
                'message' => 'Cart empty!'
            ], 422);
        }

        return $next($request);
    }
}
