<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::post('login', 'UserController@login');
    Route::post('logout', 'UserController@logout');
    Route::get('me', 'UserController@me');
});

Route::group(['prefix' => 'currency'], function () {
    Route::get('list/', 'CurrencyController@index');
    Route::get('change/{currency}', 'CurrencyController@change');
});

Route::resource('contact', 'ContactsController');

Route::resource('items', 'ItemController');
Route::resource('orders', 'OrderController');

Route::resource('cart', 'CartController', [
    'parameters'=> [
        'cart' => 'item'
    ]
]);
