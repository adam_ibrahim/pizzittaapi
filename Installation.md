## Installation

#### Clone the repo 

````
git clone https://adam_ibrahim@bitbucket.org/adam_ibrahim/pizzitta-server.git
````

#### ENV file

````
cd pizzitta-server && cp .env.example .env
````

#### Composer / Packages

````
composer install
````

#### App key

````
php artisan key:generate
````

#### Generate JWT secret key

````
php artisan jwt:secret
````

#### Database

create a local Database and make changes at .env file

once that done you can run 

````
php artisan migrate --seed
````

#### User Credentials 
There is a ready created user for testing 
````
'email':'info@pizzita.test', 'password':'password'
````


#### Tests

included Unit and Feature tests (70+ tests)

Create a local database for testing

update database credentials at .env.testing file

run 

````
php artisan config:cache --env=testing
````

the run the test 

````
php artisan test
````

#### Postman

I'm using post man for testing api requests here is a publish version of the requests
https://documenter.getpostman.com/view/2897031/SzYbzHWx?version=latest#089c8a5c-be43-4990-b2bb-763b683bead5

to use post man for local tests please import postman collection json file included in this repository
````
Pizzitta.postman_collection.json
````


you can also use the postman Environment json file included at the repository
````
Pizzitta Local.postman_environment.json
````


#### JWt Auth

I'm using JWT auth for authentication 

if testing requests with postman need to send a login request 
````
POST /auth/login, ['email':'info@pizzita.test', 'password':'Password']
````

from response you can copy the token and place it at the postman env var {{token}}


But if testing from front end then just login with auth credit 
